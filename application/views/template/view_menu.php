<table width="1072" border="3" cellspacing="0" cellpadding="10" align="center">
 
    
   <style type="text/css">
	html,body{
		padding: 0;
		margin:0;
		font-family: sans-serif;
	}
 
	.menu{
		background-color: #3141ff;
	}
 
	.menu ul {
		list-style-type: none;
		margin: 0;
		padding: 0;
		overflow: hidden;
	}
 
	.menu > ul > li {
		float: left;
	}
 
	
	.menu li a {
		display: inline-block;
		color: white;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
	}
 
	.menu li a:hover{
		background-color: #2525ff;
	}
 
	li.dropdown {
		display: inline-block;
	}
 
	.dropdown:hover .isi-dropdown {
		display: block;
	}
 
	.isi-dropdown a:hover {
		color: #fff !important;
	}
 
	.isi-dropdown {
		position: absolute;
		display: none;
		box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
		z-index: 1;
		background-color: #f9f9f9;
	}
 
	.isi-dropdown a {
		color: #3c3c3c !important;
	}
 
	.isi-dropdown a:hover {
		color: #232323 !important;
		background: #f3f3f3 !important;
	}
</style>
 
 
<header class="header">
	<div class="menu">
 
		<ul>
			<li><a href="#">Home</a></li>
			<li class="dropdown"><a href="#">Data Master</a>
			<ul class="isi-dropdown">
			<li><a href="<?=base_url();?>karyawan/listkaryawan">Data Karyawan</a></li>
			<li><a href="<?=base_url();?>jabatan/listJabatan">Data Jabatan</a></li>
			<li><a href="<?=base_url();?>barang/listBarang">Data barang</a></li>
			<li><a href="<?=base_url();?>jenis_barang/listjenisBarang">Data Jenis barang</a></li>
			<li><a href="<?=base_url();?>supplier/listsupplier">Data supplier</a></li>
			
					
				</ul>
				</li>
			<li class="dropdown"><a href="#">transaksi</a>
				<ul class="isi-dropdown">
					<li><a href="<?=base_url();?>pembelian/listpembelian"><span>list pembelian</span></a></li>
				</ul>
				<li class="dropdown"><a href="#">Report</a>
				<ul class="isi-dropdown">
					<li><a href="<?=base_url();?>pembelian/report"><span>laporan pembelian</span></a></li>
				</ul>
			
			<li><a href="<?=base_url();?>auth/logout">Logout</a></li>
			</ul>
	

 
	</div>
</header>
  </table>